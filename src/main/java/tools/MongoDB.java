/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tools;


import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import java.util.ArrayList;
import org.bson.Document;

/**
 *
 * @author Luis.Jose.local
 */
public class MongoDB {
    private MongoClient client;
    
    public MongoDB(){
        client = new MongoClient();
    }

    public ArrayList<String> getData(String database, String collection) {
        // el metodo devuelve una lista con las palabras en espaniol en este caso
        ArrayList<String>list = new  ArrayList<>();
        
      
        
        MongoDatabase db = client.getDatabase(database);
        MongoCollection documents_collection = db.getCollection(collection); //se obtiene la coleccion

        MongoCursor it = documents_collection.find().iterator(); //se crea un iterador a partir de la coleccion

        while (it.hasNext()) {
          
            
            Document document = (org.bson.Document) it.next();   //se convierten los datos que devuelve el iterador a documentos
            String name = (String) document.get("name"); //se obtiene un String del valor de la clave "name" la cual contiene la palabra en espaniol
            list.add(name); // se agrega la palabra en espaniol a la lista
        }
        return list; 
    }
    


    public void setData(String words, String database, String collection) {
    /*este metodo es llamado por el rabbitconsumer para insertar las palabras ya traducidad a mongoDB
        recibe como parametro un String llamado "words", el cual representa una cadena de palabras en ingles 
        (osea, las palabras en espaniol ya traducidas al ingles gracias a la clase IBMtranslator del paquete tools)*/
        MongoDatabase db = client.getDatabase(database);
        MongoCollection documents_collection = db.getCollection(collection);
        Document doc = new Document().append("word", words); /*se convierte el String con las palabras en ingles
        a Document, agregandose dichas palabras como valor de la clave "word"*/
        documents_collection.insertOne(doc); //finalmente, se inserta el documento con las palabras en ingles en mongoDB

    }

}
