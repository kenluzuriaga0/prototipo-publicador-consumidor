/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tools;

import com.ibm.cloud.sdk.core.security.IamAuthenticator;
import com.ibm.watson.language_translator.v3.LanguageTranslator;
import com.ibm.watson.language_translator.v3.model.TranslateOptions;
import com.ibm.watson.language_translator.v3.model.TranslationResult;
import java.util.ArrayList;

/**
 *
 * @author Luis.Jose.local
 */
public class IBMtranslator {

    private static String key = "8QIrdvXd7HhnpmuBAL_l81VTqEe5Rg2Q6WTYcCFqc1YZ";
    private static String uri = "https://api.us-south.language-translator.watson.cloud.ibm.com/instances"
            + "/5169d486-a701-4624-927c-de0924b0d3da";

    private IamAuthenticator authenticator;
    private LanguageTranslator languageTranslator;

    public IBMtranslator() {
        //el servicio se autentica en el constructor
        authenticator = new IamAuthenticator(key);
        languageTranslator = new LanguageTranslator("2018-05-01", authenticator);
        languageTranslator.setServiceUrl(uri);
    }

    
    public String translate(ArrayList<String> list) {
        //este metodo se encarga de traducir las palabras en espaniol a ingles
        TranslateOptions translateOptions = new TranslateOptions.Builder()
                .addText(joinWords(list)) //se agregan las palabras llamando al metodo "joinWords", el cual une los lotes de 100 palabras en un solo String
                .modelId("es-en") //se configura la traduccion de espaniol a ingles
                .build();

        TranslationResult result = languageTranslator.translate(translateOptions)
                .execute().getResult(); //obtiene el resultado de la traduccion

        return result.getTranslations().get(0).getTranslation(); //devuelve una representacion en String de todas las palabras traducidas
    }

    
    public String joinWords(ArrayList<String> list) {
        String words = "";
        for (String e : list) {
            words = words + " " + e;
        }
        return words;
    }

}
