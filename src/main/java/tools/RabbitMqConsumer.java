/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tools;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author Luis.Jose.local
 */
public class RabbitMqConsumer {

    private ConnectionFactory factory;
    private Connection connection;
    private ArrayList<String> list;
    private IBMtranslator translator;
    private MongoDB mongo;

    public RabbitMqConsumer() throws Exception {
         //el constructor se encarga de iniciar sesion en rabbitmq cloud colocando las credenciales pertinentes
        this.mongo = new MongoDB();
        this.list = new ArrayList<>();
        this.translator = new IBMtranslator();
        this.factory = new ConnectionFactory();
        factory.setUri("amqps://ggjmbnyq:IAELIqxaw8R8DTZFsBg9jwjyZZX9Bmvh@toad.rmq.cloudamqp.com/ggjmbnyq");
        factory.setVirtualHost("ggjmbnyq");
        factory.setUsername("ggjmbnyq");
        factory.setPassword("IAELIqxaw8R8DTZFsBg9jwjyZZX9Bmvh");
        factory.setConnectionTimeout(30000);
        connection = factory.newConnection();

    }

    public void consumeData() throws InterruptedException, IOException {
        Channel channel = connection.createChannel();
        boolean autoAck = false;

        /*el metodo "basicConsume" del objeto channel, coloca el canal a la escucha de mensajes listos en la cola
        gracias al objeto de tipo "DefaultConsumer" que se le pasa a dicho metodo por parametro. esto quiere decir
        que los mensajes llegan de manera asincronica. a medida que los productores envian datos a la cola, el canal
        los recibira asincronicamente(automaticamente)*/
        channel.basicConsume("Words", autoAck, "myConsumerTag",
                new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag,
                    Envelope envelope,
                    AMQP.BasicProperties properties,
                    byte[] body)
                    throws IOException {

                long deliveryTag = envelope.getDeliveryTag();

                String message = new String(body, "UTF-8");

                list.add(message);//se agregan los mensajes que llega (palabras en espaniol) a una lista

                if (list.size() >= 100) {  /*se verifica cuando la lista posee 100 o mas palabras
                    si ese es el caso.....*/

                    String translate_words = translator.translate(list); /*se procede a enviar la lista
                    llena de palabras en espaniol al traductor de tipo "IBMtranslator" (clase creada en el
                    paquete tools) para traducir dichas palabras a ingles y devolverlas en forma de String*/
                    
                    mongo.setData(translate_words, "words", "wordsEnglish"); /*este string de palabras en ingles
                    traducidas pasa al metodo setData de una instancia de MongoDB, aqui finalmente se guardan 
                    las palabras traducidas a ingles en la base de datos "words", coleccion "wordsEnglish" 
                    (ver la clase MongoDB en tool si quiere observar este metodo)*/
                    
                    list.clear(); //se limpian de la lista las palabras en espaniol para repetir todo el proceso de nuevo
                    //por lo que se van ingresando a la base de datos, documentos con lotes de 100 palabras traducidas

                }

                channel.basicAck(deliveryTag, false);

            }
        });

    }

  

 

  

}
