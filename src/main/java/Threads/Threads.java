/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Threads;

import java.util.ArrayList;
import tools.RabbitMQSender;

/**
 *
 * @author Luis.Jose.local
 */
public class Threads extends Thread {

    private ArrayList<String> list;

    public Threads(ArrayList<String> list) {
        this.list = list;
    }

    @Override
    public void run() {
        try {
            RabbitMQSender rbmq;
            rbmq = new RabbitMQSender(); /*se crea una instancia de RabbitMQSender (ubicada en el
            paquete tools)*/
            rbmq.insertDataToQueu(list); /*se usa el rabbitsender para enviar los datos a la cola
            (ver la clase RabbitMQSender para observar el proceso de envio)*/
        } catch (Exception ex) {

        }
    }

}

