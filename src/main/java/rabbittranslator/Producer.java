/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rabbittranslator;

import Threads.Threads;
import java.util.ArrayList;
import tools.MongoDB;


/**
 *
 * @author Luis.Jose.local
 */
public class Producer {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
     
        MongoDB db = new MongoDB(); //se crea instancia se mongoDB (clase del paquete tools)
        ArrayList<String> list = db.getData("words", "wordsSpanish"); /*se obtienen palabras en 
        espaniol de la coleccion "wordsSpanish" de la base de datos "words" */

        try {
            /*las palabras en espaniol almacenadas en la list, se envian a los hilos, esta clase
            se creo en el paquete "Threads"*/
            Threads t1 = new Threads(list);
            Threads t2 = new Threads(list);
            Threads t3 = new Threads(list);
            t1.start();
            t2.start();
            t3.start();
             /*los hilos se inician con el metodo start, cada uno de estos simula un productor
            "los sensores" los cuales envian los mensajes hacia la cola de rabbitmq llamada "words")*/
             
             //NOTA: revisar la clase Threads si se quiere ver como se envian los datos a la cola
            
        } catch (Exception ex) {

        }
         


    }

}
