/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rabbittranslator;

import java.util.ArrayList;
import org.bson.Document;
import tools.MongoDB;
import tools.RabbitMqConsumer;

/**
 *
 * @author Luis.Jose.local
 */
public class Consumer {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            RabbitMqConsumer rbmq;
            rbmq = new RabbitMqConsumer(); /*se crea una instancia de RabbitMqConsumer (ubicada en el paquete tools)*/

            rbmq.consumeData(); /*se llama al metodo "consumeData el cual colocara el canal de rabbitmq a la escucha
            de mensajes (ver clase RabbitMqConsumer para observar este proceso)"*/

        } catch (Exception ex) {

        }
        
    }
}
